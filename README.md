# React Hooks - Rick & Morty API

_Proyecto de practica del curso de react hooks de platzi. En el proyecto se consume la API de Rick & Morty para mostrar algunos de los personajes, tiene incorporada una barra de busqueda para encontrar algunos d elos personajes incluidos en la lista, tambien se les puede añadir como favoritos a una lista y eliminar de la misma. Esta aplicacion usa los principales hooks como useState, useEffect, useContext y useReducer, estos dos ultimos para manejar un estado global en la aplicacion_

![Captura de Movie React](./.readme-static/react-hooks-api.jpg)

[Ver en netlif](https://priceless-bhabha-4b3238.netlify.app)

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node js
```

### Instalación 🔧

_Clona el repositorio y una vez clonado, instalá las dependencias necesarias ejecutando el siguiente comando:_

```
npm install
```

_Levanta el proyecto con el siguiente comando:_

```
npm start
```

## Construido con 🛠️

_Herramientas utilizadas en el proyecto_

* [React](https://es.reactjs.org/) - El framework web usado
* [Rick & Morty API](https://rickandmortyapi.com/) -API utilizada en el proyecto


---
⌨️ con ❤️ por [Mario Ochoa](https://www.instagram.com/mario_8a_/) 😊