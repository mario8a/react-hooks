import { useReducer, useState, useMemo, useRef, useCallback } from "react";
import './Character.css';
import { Search } from './Search';
import useCharacters from './../hooks/useCharacter';
import { favoriteReducer } from "../reducer/favoriteReducer";

const initialState = {
   favorites: []
}

const API = 'https://rickandmortyapi.com/api/character/';

export const Characters = () => {

   // const [characters, setCharacters] = useState([]);
   const [favorites, dispatch] = useReducer(favoriteReducer, initialState);
   const [search, setSearch] = useState('');
   const searchInput = useRef(null);

   // useEffect(() => {
   //    fetch('https://rickandmortyapi.com/api/character/')
   //      .then(res => res.json())
   //      .then(data => setCharacters(data.results));
   //  }, []);

   //Usando custom hook
   const characters = useCharacters(API);


    const handleClick = favorite => {
       dispatch({
          type: 'ADD_TO_FAVORITE',
          payload: favorite
       })
    }

    const handleDeleteFav = favorite => {
       dispatch({
          type: 'DELETE_FAVORITE',
          payload: favorite
       })
    }
   //  Funcion para manejar la busqueda
   // const handleSearch = (e) => {
   //    // setSearch(e.target.value);
   //    setSearch(searchInput.current.value)
   // }

   //Implementando ek useCallback
   const handleSearch = useCallback(() => {
      setSearch(searchInput.current.value)
   }, []);

   // const filteredUsers = characters.filter((user) => {
   //    return user.name.toLowerCase().includes(search.toLocaleLowerCase());
   // });

   //Usando useMemo para memorizar los filtrados
   const filteredUsers = useMemo(() => 
      characters.filter((user) => {
         return user.name.toLowerCase().includes(search.toLocaleLowerCase());
      }),
      [characters, search]
   );

   return (
      <>
      <Search search={search}
             searchInput={searchInput}
             handleSearch={handleSearch}
       />
      <hr />
      <div className="characters-container">
         {
            favorites.favorites.map(favorite => (
               <div key={favorite.id} className="card animate__animated animate__fadeIn">
                  <div className="card__img-container">
                     <img src={favorite.image} alt="Imagen del personaje" />
                  </div>
                  <div className="card__info-container">
                     <h3> {favorite.name} </h3>
                  </div>
                  <button className="btn-fav" type="button" onClick={() => handleDeleteFav(favorite)}>
                     Delete Fav
                  </button>
               </div> 
            ))
         }
      </div>
      <hr />
      <div className="characters-container">
         {
            // characters
            filteredUsers.map(character => (
               <div key={character.id} className="card animate__animated animate__fadeIn">
                  <div className="card__img-container">
                     <img src={character.image} alt="Imagen del personaje" />
                  </div>
                  <div className="card__info-container">
                     <h3> {character.name} </h3>
                  </div>
                  <button className="btn-fav" type="button" onClick={() => handleClick(character)}>
                     Add fav
                  </button>
               </div>        
            ))
         }
      </div>
      </>
   )
}
