import React from 'react';
import './Search.css';

export const Search = ({search, searchInput, handleSearch}) => {
   return (
      <div className="search">
         <div className="search-container">
            <input type="text" value={search} ref={searchInput} onChange={handleSearch} placeholder="Rick Sanchez"/>
         </div>
      </div>
   )
}
