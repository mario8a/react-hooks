import { useState, useContext } from "react";
import ThemeContext from './../context/ThemeContext';



export const Header = () => {
   // [variable, funcion para cambiar el estado de desa variable]
   const [darkmode, setDarkmode] = useState(false);
   const { darkMode } = useContext(ThemeContext);

   const handleClick = () => {
      setDarkmode(!darkmode);
   };

   return (
      <div className={ !darkmode ? darkMode : 'header'}>
         <h1 >Rick & Morty</h1>
        <div>
            <button type="button" onClick={handleClick}>
               {darkmode ? "Dark Mode" : "Light mode"}
            </button>
        </div>
      </div>
   );
};
